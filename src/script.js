/**
 * Code documentation coverage logic and state store.
 *
 * @param {Object} data       initial docs with the files they cover
 * @param {String} template   template to render
 * @param {String} el         id of the target DOM elemnt
 */
function SwimmTemplateRenderer(data, template, el) {
  var files = {};
  Object.keys(data.docs).forEach((doc) => {
    if (!(doc in files)) {
      files[doc] = new Array();
    }
    docs[doc].forEach((fileName) => {
      if (!files[doc].includes(fileName)) {
        files[doc].push(fileName);
      }
    });
  });
  var self = this;
  /**
   * docs stored via porxy, on change relevant (and only relevant) DOM elemnts are rerenderd.
   */
  let docsProxy = new Proxy(data.docs, {
    get: function (target, prop) {
      return target[prop];
    },
    /**
     * on new doc insert _renderItem called and appending elemnt to DOM (Doc row).
     * on existing doc update _renderItemValue called and updating existing DOM elemnt value.
     */
    set: function (obj, prop, value) {
      value.forEach((file) => {
        if (file in self.data.files) {
          if (!self.data.files[file].includes(prop)) {
            let oldVal = JSON.parse(JSON.stringify(self.data.files[file]));
            self.data.files[file].push(prop);
            self._renderItemValue(
              "files-" + file,
              self.data.files[file],
              oldVal
            );
          }
        } else {
          self.data.files[file] = new Array();
          self.data.files[file].push(prop);
          self._renderItem(
            "files",
            file,
            self.data.files[file],
            '<li class="list-group-item"><button type="button" class="btn btn-primary">{{key}}: <span class="badge bg-secondary">0</span></button> {{value}}</li>'
          );
        }
      });
      obj[prop] = value;
    },
  });

  this.data = {
    docs: docsProxy,
    files: files,
  };
  this.filesInputCount = 1;
  this.template = template;
  this.el = el;
  this._init();
  this._render();
}

SwimmTemplateRenderer.prototype._init = function () {};

/**
 * update (and initiate) existing item value (file doc list)
 *
 * @param {String} id       id of the item DOM elemnt to render
 * @param {String} newVal   value to display for item
 * @param {String} oldVal   corrent value of the item
 */
SwimmTemplateRenderer.prototype._renderItemValue = function (
  id,
  newVal,
  oldVal
) {
  let cell = document.getElementById(id);
  cell.innerHTML = cell.innerHTML.replace(oldVal, newVal);
  const badgeForRegex = /<span class="badge bg-secondary">[0-9]*<\/span>/g;

  cell.innerHTML = cell.innerHTML.replace(
    badgeForRegex,
    `<span class="badge bg-secondary">${newVal.length}</span>`
  );
};

/**
 * create and render new row for documented file
 *
 * @param {String} dataPoint    data sorce to take item from
 * @param {String} key          items key
 * @param {String} value        items value
 * @param {String} tmplt        template to render item by
 */
SwimmTemplateRenderer.prototype._renderItem = function (
  dataPoint,
  key,
  value,
  tmplt
) {
  const id = dataPoint + "-" + key;
  const list = document.getElementById(dataPoint);

  list.innerHTML +=
    `<div id=${id}>` + tmplt.split("{{key}}").join(key) + `</div>`;
  this._renderItemValue(id, value, "{{value}}");
};

/**
 * called only on init, render target DOM elemnt. Uses regex to find relevant DOM elemnts.
 *
 */
SwimmTemplateRenderer.prototype._render = function () {
  const swimmForRegex =
    /<swimm for \(key, value\) in (.*?)>\n(.*)\n\s\s*?<\/swimm>/g;

  let renderedTemplate = this.template.replaceAll(
    swimmForRegex,
    '<div id="$1"></div>'
  );

  document.getElementById(this.el).innerHTML = renderedTemplate;

  const matches = [...this.template.matchAll(swimmForRegex)];
  for (var match in matches) {
    const dataPoint = matches[match][1];
    const tmplt = matches[match][2];
    const keyCells = Object.keys(this.data[dataPoint]).map((key) =>
      this._renderItem(dataPoint, key, this.data[dataPoint][key], tmplt)
    );
  }
};

/**
 * called on add doc submit, if input is valid add doc to state
 *
 */
SwimmTemplateRenderer.prototype.addDoc = function () {
  var form = document.querySelectorAll(".needs-validation")[0];
  form.classList.add("was-validated");
  if (!form.checkValidity()) return;

  let fileList = new Array();
  for (const i of Array(this.filesInputCount).keys()) {
    let fileNameInput = document.getElementById(`input-file-${i + 1}`);
    fileList.push(fileNameInput.value);
  }
  let docName = document.getElementById("newDocName").value;
  this.data.docs[docName] = fileList;
  document.getElementById("newDocName").value = "";
  this.filesInputCount = 1;
  const list = document.getElementById("files-input-list");
  list.innerHTML = `<input id="input-file-${this.filesInputCount}" type="text" class="form-control mb-3" placeholder="Enter Doc name" required>`;
  form.classList.remove("was-validated");
};

/**
 * adding new file name line to input form
 *
 */
SwimmTemplateRenderer.prototype.addInputFile = function () {
  this.filesInputCount += 1;
  const list = document.getElementById("files-input-list");

  var newInput = document.createElement("div");
  newInput.innerHTML = `<input id="input-file-${this.filesInputCount}" type="text" class="form-control mb-3" placeholder="Enter Doc name" required>`;
  list.appendChild(newInput.firstChild);
};

const docs = {
  "src/app.js": ["Doc1", "Doc2"],
  "src/app.spec.js": ["Doc1"],
  "webpack.config.js": ["Doc1"],
};

let data = {
  docs,
};

let template = `
<div class="card mx-auto mt-5 p-3 w-50">
		<div class="caption h4">Documentation Coverage</div>
  	  <ul class="list-group list-group-flush">
        <swimm for (key, value) in files>
          <li class="list-group-item"><button type="button" class="btn btn-primary">{{key}}: <span class="badge bg-secondary">0</span></button> {{value}}</li>
        </swimm>
		</ul>
</div>
<div class="card mx-auto mt-5 p-3 w-25">




<form class="row g-3 needs-validation" novalidate>
  <div>
    <label for="validationCustom01" class="form-label">Doc Name</label>
    <input id="newDocName" type="text" class="form-control" placeholder="Enter Doc name" required>
  </div>

<div class="form-group">
<label for="validationCustom01" class="form-label">File List</label>
    <div id="files-input-list">
    <input id="input-file-1" type="text" class="form-control mb-3" placeholder="Enter Doc name" required>
    </div>
    <button class="btn text-white btn-success w-100" onClick="coverage.addInputFile()">Add file</button>
    <small id="emailHelp" class="form-text text-muted">Enter full file's path.</small>
</div>
    <button onClick="coverage.addDoc()" class="btn text-white mt-3 w-100" style="background-color:#10243c;margin:auto">Submit</button>
</form>

</div>
`;

let el = "app";

//create initial instance of SwimmTemplateRenderer with default data
var coverage = new SwimmTemplateRenderer(data, template, el);
